# Locals

Usa-se este arquivo para definir definir valores específicos que podem ser baseados em uma variável ou montar outro.

Observe o bloco abaixo. Por exemplo a variável timestamp ela recebe um valor em em tempo de execução vinda de uma função. Extra tags foi definida baseada em uma variáveis.

Gosto de separar esse arquivo para não ficar poluindo o arquivo principal. Geralmente interpolação para gerar uma nova variável prefiro fazer neste arquivo e referenciar essas variáveis no main.pkr.hcl de forma simples e mais legível.

>Pense em locals como seu espaço se manipulação de variáveis.

O arquivo abaixo é exatamente o que vamos usar no nosso projeto.

```hcl
locals { 
    timestamp = formatdate("DD-MMM-YYYY-hh-mm-ZZZ", timestamp())

    extra_tags = {
        Name = var.project_name
        Environment = var.environment
    }
}
```

uma variável é referencia como var.nomedela e uma variavel local é referenciada como local.nomedela.

Aproveitando este momento é bom falar de funções que ajudam bastante a manipular variáveis. Siga a [documentação](https://developer.hashicorp.com/packer/docs/templates/hcl_templates/functions) e veja tudo que temos pronto pra uso.

Se já é familiariazado com o terraforma é a mesma coisa.

Lista das que mais uso:

- env: Transformar uma variável de ambiente em um valor de entrada.

    ```hcl
    env("AWS_DEFAULT_REGION")
    ```

- vault: Busca um valor em algum cofre do vault, desde que esteja logado.

    ```hcl
    vault("/secret/data/hello", "foo")
    ```

- join: Junta varias strig com um separador, ótimo pra formar nomes.

    ```hcl
    join("_", [var.project_name, var.environment, var.region]) \
    centos_dev_us-east-1
    ```

- split: Separa uma string através de um delimitador e entrega uma lista de strings. É o inverso do join

- lower, upper e title: Converte todas as letras para tudo minusculo, maiusculo ou somente a primeira letra maiucuslo no caso de title.

    ```hcl
    lower("HELLO") \
    hello
    ```

- regex e regex_replace: São duas funções coringas que para quem saber usar regex, substitui muitas outras funções

- coalesce: e coalestlist: De uma lista de argumentos pega o primeiro não nulo.

- concat: Combina duas listas em uma lista

  ```hcl
  `concat(["a", ""], ["b", "c"]) \
   ["a","","b","c",]
  ```

- contains: Dentro de uma lista verifica se um item exite e retorna true ou false.
  
  ```hcl
  contains(["a", "b", "c"], "a")
  true
  ```

- Index e element: São o inverso uma da outra. element() vc passa a lista e o index ele te retorna o valor na posição da lista. index() vc passa a lista e o valor ele te retorna em qual posição da lista esta.

  ```hcl
  element(["a", "b", "c"], 1) \
  b
  index(["a", "b", "c"], "b") \
  ```

- lookup: Recupera o valor de um único elemento de um map, dada sua key. Se a chave fornecida não existir, o valor padrão fornecido será retornado.

  ```hcl
  lookup({a="ay", b="bee"}, "a", "DefaultValue") \
  ay
  lookup({a="ay", b="bee"}, "c", "DefaultValue")
  DefaultValue
  ```

- merge: Pega um número arbitrário de mapas e retorna um único mapa que contém um conjunto mesclado de elementos de todos os mapas. Se mais de um determinado mapa definir a mesma chave, aquele que estiver mais tarde na sequência de argumentos terá precedência.

  ```hcl
  merge({"a"="b", "c"="d"}, {"e"="f", "c"="z"}) \
  {"a" = "b" "c" = "z" "e" = "f"}
  ```
  
- jsondecode e yamldecode: São funções que possuem também o encode, mas geralmente nós temos o arquivo e queremos.

- file: le o conteúdo de um arquivo passado no path relativo. fileexists() é uma outra função que retorna true ou false.

  ```hcl
  file("${path.folder}/hello.txt") \
  Hello World
  ```

- timestap: retorna a data e hora. Geralmente usado com o formatedate para personalizar a hora de acordo com o que vc precisa.

Existm muitas outras, vale a pena conferir.

## Extra

path.cwd: o diretório de onde o Packer foi iniciado.

path.root: o diretório do arquivo HCL de entrada ou a pasta de entrada.
