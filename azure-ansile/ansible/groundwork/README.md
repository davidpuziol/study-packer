Role Name
=========

Groundwork will prepare vm

Requirements
------------

Nothing

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Only Built In Ansible

License
-------

Copyright (c) 2023 Wind River Systems, Inc.

The right to copy, distribute, modify, or otherwise make use of this software may be licensed
only pursuant to the terms of an applicable Wind River license agreement.

Author Information
------------------

David Puziol Prata
