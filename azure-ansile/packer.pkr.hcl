packer {
  required_version = ">= 1.8.5"
  required_plugins {
    azure = {
      version = ">= 1.4.1"
      source  = "github.com/hashicorp/azure"
    }
  }
}