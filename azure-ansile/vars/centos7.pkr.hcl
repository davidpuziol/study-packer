image_name  = "myimage"
environment = "develop"

region = "us_oregon"

tags = {
  OS_Version    = "Centos 7"
  Release       = "Latest"
  Base_AMI_Name = "{{ .SourceAMIName }}"
  Extra         = "{{ .SourceAMITags.TagName }}"
}

image_publisher = "center-for-internet-security-inc"
image_offer     = "cis-centos-7-v2-1-1-l1"
image_sku       = "cis-centos7-l1"
