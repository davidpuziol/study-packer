
source "azure-arm" "azure" {
  # client_id       = var.client_id
  # client_secret   = var.client_secret
  # tenant_id       = var.tenant_id
  # subscription_id = var.subscription_id

  use_azure_cli_auth = true

  plan_info {
    plan_publisher = var.image_publisher
    plan_product   = var.image_offer
    plan_name      = var.image_sku
  }

  os_type         = "Linux"
  image_publisher = var.image_publisher
  image_offer     = var.image_offer
  image_sku       = var.image_sku

  managed_image_resource_group_name = "packer"
  managed_image_name                = local.full_image_name

  azure_tags = var.vm_tags

  location = local.map_regions[var.region]["azure"]
  vm_size  = var.instance_sizes["azure"]
}

build {

  name = var.image_name

  sources = ["source.azure-arm.azure"]

  provisioner "file" {
    destination = "~/"
    source      = "./ansible"
  }

  ## Instalando o ansible com sudo
  provisioner "shell" {
    execute_command = "{{ .Vars }} sudo -E /bin/bash '{{ .Path }}'"
    inline = [
      "yum install -y -q epel-release",
      "yum-config-manager --enable epel",
      "yum -y install https://packages.endpointdev.com/rhel/7/os/x86_64/endpoint-repo.x86_64.rpm",
      "sudo yum install -y -q ansible",
    ]
  }
  ## Instalando collections necessárias e rodando o playbook
  provisioner "shell" {
    execute_command = "{{ .Vars }} /bin/bash '{{ .Path }}'"
    inline = [
      "ansible-galaxy collection install -r ~/ansible/requirements.yml",
      "ansible-playbook ~/ansible/main.yml"
    ]
  }

  ## Script necessário para finalizar a máquina
  provisioner "shell" {
    only            = ["source.azure-arm.azure"]
    execute_command = "chmod +x {{ .Path }}; {{ .Vars }} sudo -E sh '{{ .Path }}'"
    inline          = ["cp -p -r /tmp/linux /bin/demo", "/usr/sbin/waagent -force -deprovision+user && export HISTSIZE=0 && sync"]
    inline_shebang  = "/bin/sh -x -e"
  }
}
