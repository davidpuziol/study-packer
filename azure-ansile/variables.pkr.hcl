variable "instance_sizes" {
  description = "Instante type to create the image"
  type        = map(string)
  default = {
    aws   = "t3a.2xlarge"
    azure = "Standard_D8s_v3"
  }
}

variable "environment" {
  type        = string
  description = "Environment"
  validation {
    condition = contains([
      "develop",
      "staging",
      "production"
    ], var.environment)
    error_message = <<-EOF
    Invalid input, options: develop, staging or production.
    EOF
  }
}

variable "region" {
  description = "Locations to map regions on each cloud"
  type        = string
  validation {
    condition = contains([
      "us_california",
      "us_ohio",
      "us_oregon",
      "ireland",
      "london",
      "france",
      "sweden",
      "germany",
      "frankfurt",
      "brazil",
      "canada",
      "japan",
      "korea",
      "australia",
      "india",
      "singapore",
    ], var.region)
    error_message = <<-EOF
      Invalid input, options:
        us_california
        us_ohio
        us_oregon
        ireland
        london
        france
        sweden
        germany
        frankfurt
        brazil
        canada
        japan
        korea
        australia
        india
        singapore
        .
    EOF
  }
}

variable "vm_tags" {
  description = "A map of tags for the image"
  type        = map(string)
  default = {
    CreatedBy = "Packer"
  }
}


variable "image_name" {
  description = "Future Ami Name"
  type        = string
}

variable "image_tag" {
  description = "Tag to concat in image_name"
  type        = string
  default     = ""
}


#### AZURE Variables
variable "client_id" {
  description = "AZURE Client ID"
  type        = string
  default     = env("AZURE_CLIENT_ID")
}

variable "client_secret" {
  description = "AZURE Client Secret"
  type        = string
  default     = env("AZURE_CLIENT_SECRET")
}

variable "tenant_id" {
  description = "AZURE Tenant ID"
  type        = string
  default     = env("AZURE_TENANT")
}

variable "subscription_id" {
  description = "AZURE Subscription ID"
  type        = string
  default     = env("AZURE_SUBSCRIPTION_ID")
}

variable "image_publisher" {
  description = "Azure image publisher"
  type        = string
}

variable "image_offer" {
  description = "Azure Image Offer"
  type        = string
}

variable "image_sku" {
  description = "Image specific version"
  type        = string
}