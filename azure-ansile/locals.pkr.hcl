locals {
  timestamp = formatdate("DD-MMM-YYYY-hh-mm-ZZZ", timestamp())

  full_image_name = var.image_tag == "" ? "${var.image_name}-${local.timestamp}" : "${var.image_name}-${var.image_tag}"

  extra_tags = {
    Name = var.image_name
  }

  map_regions = {
    ireland = {
      aws   = "eu-west-1"
      azure = "North Europe"
      gcp   = "europe-west1" // Belgium
    }
    london = {
      aws   = "eu-west-2"
      azure = "UK South"
      gcp   = "europe-west2"
    }
    france = {
      aws   = "eu-west-3"
      azure = "France Central"
      gcp   = "europe-west9"
    }
    us_ohio = {
      aws   = "us-east-2"
      azure = "East US 2"
      gcp   = "us-east5"
    }
    us_california = {
      aws   = "us-west-1"
      azure = "West US 2" // Washington
      gcp   = "us-west2"
    }
    us_oregon = {
      aws = "us-west-2"
      # azure = "West US 3" // Arizona
      azure = "West US 2"
      gcp   = "us-west1"
    }
    sweden = {
      aws   = "eu-north-1"
      azure = "Sweden Central"
      gcp   = "europe-north1" // Finland
    }
    germany = {
      aws   = "eu-central-1"
      azure = "Germany West Central"
      gcp   = "europe-west3"
    }
    frankfurt = {
      aws   = "eu-central-1"
      azure = "Germany West Central"
      gcp   = "europe-west3"
    }
    brazil = {
      aws   = "sa-east-1"
      azure = "Brazil South"
      gcp   = "southamerica-east1"
    }
    canada = {
      aws   = "ca-central-1"
      azure = "Canada Central" // Toronto
      gcp   = "northamerica-northeast1"
    }
    japan = {
      aws   = "ap-northeast-1"
      azure = "Japan East"
      gcp   = "asia-northeast1"
    }
    korea = {
      aws   = "ap-northeast-2"
      azure = "Korea Central"
      gcp   = "asia-northeast3"
    }
    australia = {
      aws   = "ap-southeast-2"
      azure = "Australia East" // New South Wales
      gcp   = "australia-southeast1"
    }
    india = {
      aws   = "ap-south-1"
      azure = "Central India"
      gcp   = "asia-south1"
    }
    singapore = {
      aws   = "ap-southeast-1"
      azure = "Southeast Asia"
      gcp   = "asia-southeast1"
    }
  }
}