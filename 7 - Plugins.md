# Plugins

Os plug-ins do Packer são aplicativos separados e independentes que executam tarefas durante cada compilação.

Durante um `packer build`, a lista de processos mostra `packer-` para aplicações prefixas. Um desses aplicativos é o binário do Packer e o restante são plug-ins. O Packer inicia um processo de plug-in para cada componente na compilação.

O plugins tem níveis diferentes

| Nível | Descrição | Namespace |
|---|---|---|
|  Oficial | Plugins oficiais são de propriedade e mantidos pela HashiCorp. | hashicorp |
|  Verificado | Os plug-ins verificados pertencem e são mantidos por parceiros de tecnologia terceirizados,mas verificados pela HashiCorp | Organização terceirizada |
| Comunidade | Providos pela comunidade. |  |
| arquivado | Não são mais mantidos pela HashiCorp, um parceiro ou pela comunidade. | hashicorp ou terceiros |

O binário do Packer possui um conjunto de plug-ins integrados que podem ser encontrados e executados automaticamente. Você também pode definir uma lista de plug-ins externos em seu modelo para o Packer executar e se comunicar durante a compilação. Esses plug-ins externos estendem a funcionalidade do Packer sem modificar o código-fonte principal.

Esses plugins devem ser definidos dentro do bloco packer. Por isso gosto de separar em packer.pkr.hcl.

Confira a [lista](https://developer.hashicorp.com/packer/plugins) dos plugins externos

Os mais importantes:

- Ansible (Oficial)
- Amazon EC2 (Oficial)
- Azure (Oficial)
- [Git](https://developer.hashicorp.com/packer/plugins/datasources/git)

No nosso estudo vamos utilizar o plugin da [amazon](https://github.com/hashicorp/packer-plugin-amazon) e da [azure](https://github.com/hashicorp/packer-plugin-azure). É necessário defini-los dentro do bloco packer, mas gosto de separar esse bloco no arquivo packer.pkr.hcl.

```properties
packer {
  required_version = ">= 1.8.5"
  required_plugins {
    amazon = {
      version = ">= 1.2.1"
      source  = "github.com/hashicorp/amazon"
    }
    azure = {
      version = ">= 1.4.1"
      source  = "github.com/hashicorp/azure"
    }
  }
}
```

O comando `packer init .` irá fazer o download desses plugins. Esses plugins por padrão são salvos em ~/.config/packer/plugins e não no projeto como o terraform faz com o .terraform. Uma vez que o plugin esteja baixado na versão correta outras execuções não irão baixá-lo novamente se a versão for compatível.

```bash
~/.config/packer                                                                                                 1.3.7 02:06:06
❯ tree
.
├── checkpoint_cache
├── checkpoint_signature
└── plugins
    └── github.com
        └── hashicorp
            ├── amazon
            │   ├── packer-plugin-amazon_v1.2.1_x5.0_linux_amd64
            │   └── packer-plugin-amazon_v1.2.1_x5.0_linux_amd64_SHA256SUM
            └── azure
                ├── packer-plugin-azure_v1.4.1_x5.0_linux_amd64
                └── packer-plugin-azure_v1.4.1_x5.0_linux_amd64_SHA256SUM
```
