
# Comunicator é obrigatório pode ser ssh winrm ou none no caso de null
# Dependendo do source source ssh deve ser definido por exemplo; no caso da aws e no caso da azure ssh é o padrão uma vez que todas as máquinas tem o user azure.
variable "pass" {
  description = "My password"
  type = string
  default = env("MY_PASS")
}

source "null" "first-example" {
  communicator = "none"
}

# Pode ter vários sources do mesmo tipo, mas nunca com o mesmo nome
source "null" "second-example" {
  ssh_host= "127.0.0.1"
  ssh_username = "david"
  ssh_password = var.pass
  # O argumento abaixo é necessário mas eu vou setar ele lá no bloco build
  # Se esse argumento for setado aqui não é possível setar lá.
  # ssh_password = "xxxxxx" 
}

build {
  name = "MeuNullBuild"
  # usando o primeiro construtor e adicionando a variável name
  source "null.first-example" {
    name = "consul"
  }
  # usando o primeiro construtor e adicionando uma variável extra
  source "null.first-example" {
    name = "nomad"
  }
  source "null.second-example" {
    name = "vault"
  }

  sources = ["null.first-example","null.second-example"]

  # É possível referenciar name e type dos sources da seguinte maneira
  # Não é possível referenciar outras variávies como othername usando source.othername
  provisioner "shell-local" {
    environment_vars = [
      "MY_TEST=david",
      "MY_TEST2=david2"
    ]
    inline = [
      "echo ${source.name} and ${source.type} TEST PROVISIONER",
      "echo $MY_TEST",
     ]
  }
  
  post-processor "shell-local" {
    inline = ["echo ${source.name} OR ${source.type} TEST POST-PROCESSOR" ]
  }
}