# Exemplo AWS

O projeto é subir criar uma imagem de um ubuntu Hardening na AWS utilizando scripts e respeitando a estrutura do projeto e pastas. Poderiamos fazer com o ansible, mas deixa pra outro exemplo mais simples.

A documentação sobre como utilizar esse source está em [EBS](https://developer.hashicorp.com/packer/plugins/builders/amazon/ebs)

O projeto completo está na pasta [bastion-ubuntu](https://gitlab.com/davidpuziol/study-packer/-/tree/main/bastion-ubuntu/)

## Detalhes importantes sobre o projeto

- É necessário que a vpc default esteja habilitada pois a instancia precisa subir em alguma vpc. Caso a vpc não esteja habilitada seria necessário passar algums parâmetros a mais como o vpc_id, subnet_id (pública), etc. Como a AWS não cobra pela vpc padrão mantenha habilitada que é mais fácil. Se colocasse isso no projeto ficaria mais difícil o estudo para outros.
- O source ficou reutilizável podendo ser utilizado em muitas situações.
- Os arquivos de entrada para as variáveis ficam nas pasta vars.
- Se quiser colocar uma instancia maior para fazer o build fique a vontade, esta máquina terminará rápidamente não gerando custo muito alto melhorando a velocidade no build. Por esse motivo não vejo necessidade de declarar filtros para buscar instancias spot a não ser que tenha imagem buildada todo dia, o que não é o caso.
- Usar variáveis de ambiente possibilita colocar em um pipeline.

## o que poderia ser melhorado?

- Poderia ser colocado mais amis de entrada
- Nesse caso de um bastion session manager poderia ser habilitado
- No caso de uma distribuição de imagem outras fatores devem ser levados em consideração como por exemplo licença.
- Definir iam com roles e politicas corretas para somente buildar a imagem.
- etc

## Como buildar o projeto?

Com o terminado na pasta do projeto e as variáveis da aws exportadas

```bash
packer init . --upgrade
packer build --machine-readable -var-file="./vars/ubuntu.auto.pkrvars.hcl" . | tee build.log
```
