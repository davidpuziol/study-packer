# Variaveis

[https://developer.hashicorp.com/packer/docs/templates/hcl_templates/variables](https://developer.hashicorp.com/packer/docs/templates/hcl_templates/variables)

As variáveis no packer são praticamente igual ao terraforms.

Um bloco de variáveis pode ter os seguintes argumentos

- default: Valor padrão caso não definido, é opcional.
- type: toda variável deve ser do tipo string number ou bool
  - string
  - number
  - bool
  - Mas pode ser também um conjunto delas

  ```bash
  list(`<TYPE>`)
  set(<`TYPE>`)
  map(`<TYPE>`)
  object({`<ATTR NAME>` = `<TYPE>`, ... })
  tuple([`<TYPE>`, ...])
  ```

- description - Documentação da variável, é boa prática utilizar, mas não é obrigatório.
- validation - Bloco que valida se o valor da variável pode ou não ser utilizado. Não é obrigatório.
- sensitive - Caso o valor da varíavel não deva ser exibida no console defina como true. O padrão é false e não é obrigatório.

Vejamos este exemplo. O nome da variável fica entre aspas, minusculo e deve ser usado _ ao inves de - caso o nome for composto.

```hcl
variable "enviroment" {
  type        = string
  description = "Nome do ambiente"
}
variable "instance_type" {
  type        = string
  description = "Instancia que será utilizada para processamento das configurações"
  default     = "t3.medium"
}
```

Agora imaginemos que environment pode ser somente develop, staging ou production, o que fazer? Nesse caso estamos colocando uma condicional com um erro caso não for estabelecido o valor `develop` `stating` ou `production`.

```hcl
variable "environment" {
  type        = string
  description = "Ambiente que a imagem será usada"
  validation {
    condition = contains(["develop", "staging", "production"], var.environment)
    error_message = "Invalid input, options: \"development\", \"staging\" or \"production\"."
  }
}
```

Neste podemos observer que contains é uma função que tem como entrada uma lista de possíveis palavras e irá retornar true caso algumas delas esteja na variável var.environment que for passada.

Funções são muito úteis e podem ser encontradas na documentação oficial. Farei uma lista das funções mais usadas mais pra frente.

Confira agora o que temos em `project/variables.pkr.hcl`

Em complemento temos o que usaremos de entrada nestas variáveis em `project/vars`

Observe que posso redefinir um valor de variável que já tem um valor default.

A vantagem de trabalhar com um arquivo para injetar valores nas variáveis é que caso eu queira ter 10 arquivos diferentes desses eu posso somente chamar o a função build passando o que eu quiser.

Posso ter uma entrada para produção, uma para develop e outra para staging.

## Extra

Variávels de Path

- path.cwd: o diretório de onde o Packer foi iniciado.
- path.root: o diretório do arquivo HCL de entrada ou a pasta de entrada.

Algumas variáveis são especiais e existentes que podem ser referenciadas no build em relação ao source. Isso será explicado em [build](./8%20-%20Build.md).
