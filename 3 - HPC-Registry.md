# HCP-Registry

O HCP Registry é um um registry para as imagens geradas pelo Packer. Funciona como um meio termo entre.

O registro do HCP Packer armazena metadados sobre suas imagens, incluindo quando elas foram criadas, onde a imagem existe na nuvem e qual (se houver) git commit está associado à construção da sua imagem. Você pode usar o registro para rastrear informações sobre as imagens produzidas por seu Packer, designar claramente quais imagens são apropriadas para ambientes de teste e produção e consultar as imagens douradas certas para usar nas configurações do Packer e do Terraform.

Diagrama
![Packer Registry](./pics/hcp-packer-workflow-diagram.svg)

A presença de um hcp_packer_registry no bloco build ativará o modo HCP Packer.

O Packer enviará todos os builds dentro desse bloco de build para o registro remoto se as credenciais HCP apropriadas forem definidas ( HCP_CLIENT_ID e HCP_CLIENT_SECRET). Se nenhuma credencial HCP for definida, o Packer falhará no build e sairá imediatamente antes mesmo de executar o build.

Para conseguir as credenciais crie uma conta no [portal da hashicorp](https://portal.cloud.hashicorp.com/?utm_source=learn)

Crie um repo
![repo](./pics/createrepo.png)

![repo](./pics/createiam.png)

![repo](./pics/createiam2.png)

![repo](./pics/createiam3.png)

Exporte as variáveis no seu shell (recomendo colocar dentro de .bashrc ou .zshrc) e salve-as para uso futuro.

```bash
export HCP_CLIENT_ID=xxxxxxxxxxx
export HCP_CLIENT_SECRET=xxxxxxxxxxxx
```

Dentro do bloco build se for declarado por exemplo

```hcl
build {
  hcp_packer_registry {
    bucket_name = "teste"
    description = "Teste de descrição"
    bucket_labels = {
      "owner"          = "david"
      "os"             = "centos",
      "ubuntu-version" = "Centos 7",
    }

    build_labels = {
      "build-time"   = timestamp()
      "build-source" = basename(path.cwd)
    }
  }
}
```
