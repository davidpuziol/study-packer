#!/bin/bash
set -eux -o pipefail

sudo apt-get clean
sudo apt-get autoclean
sudo apt-get -y update
sudo apt-get -y install git net-tools procps --no-install-recommends

cd /tmp || exit 1

cd ./hardening || exit 1

sed -i.bak "s/CHANGEME='.*/CHANGEME='Ubuntu-22.04'/" ./ubuntu.cfg
sed -i.bak "s/VERBOSE='.*/VERBOSE='Y'/" ./ubuntu.cfg
sed -i.bak "s/KEEP_SNAPD='.*/KEEP_SNAPD='Y'/" ./ubuntu.cfg

# Não rodar o aide
sed -i "s/  f_aide/# f_aide/g" ubuntu.sh

# Não fazer a configuracao de ssh
sed -i "s/  f_sshconfig/# f_sshconfig/g" ubuntu.sh
sed -i "s/  f_sshdconfig/# f_sshdconfig/g" ubuntu.sh

# Não fazer a configuracao de login
sed -i "s/  f_logindconf/# f_logindconf/g" ubuntu.sh
sed -i "s/  f_logindefs/# f_logindefs/g" ubuntu.sh

# Não fazer a configuracao do adduser
sed -i "s/  f_adduser/# f_adduser/g" ubuntu.sh

chmod a+x ./ubuntu.sh
sudo bash ./ubuntu.sh

cd /tmp || exit 1

rm -rvf /tmp/hardening

# AWS Session Manager
sudo useradd --user-group ssm-user
echo "ssm-user ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers.d/ssm-user

# Manage access via AWS by default
sudo ufw disable
sudo snap restart amazon-ssm-agent