#!/bin/bash

# shellcheck disable=1090
# shellcheck disable=2009
# shellcheck disable=2034

set -u -o pipefail
# -u faz com que um erro encerro o scrip
# -o pipefail evita mascarar erros durante um pipeline, pois por padrão ele somenteo ultimo é executado

# Conferindo se o bash esta em execução
if ! ps -p $$ | grep -si bash; then
  echo "Esse script requer o bash."
  exit 1
fi

# Conferindo se o systemctl esta presente
if ! [ -x "$(command -v systemctl)" ]; then
  echo "Systemctl é requerido para esse hardening"
  exit 1
fi

# Inicialização do programa
function main {
  clear

  #Comando arp e w são requeridos
  REQUIREDPROGS='arp w'
  REQFAILED=0
  for p in $REQUIREDPROGS; do
    if ! command -v "$p" >/dev/null 2>&1; then
      echo "$p is required."
      REQFAILED=1
    fi
  done

  #Se os comandos arp ou w não forem encontrados
  if [ $REQFAILED = 1 ]; then
    echo 'net-tools e procps packages precisam ser instalados.'
    exit 1
  fi

  # Pegando o caminho ds programas
  ARPBIN="$(command -v arp)"
  WBIN="$(command -v w)"
  LXC="0"


  # Se o comando resolvctl tiver saida pegar o ip do sistema por ele, senão pegar pelo resolv.conf
  if resolvectl status >/dev/null 2>&1; then
    SERVERIP="$(ip route get "$(resolvectl status |\
      grep -E 'DNS (Server:|Servers:)' | tail -n1 |\
      awk '{print $NF}')" | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' |\
      tail -n1)"
  else
    SERVERIP="$(ip route get "$(grep '^nameserver' /etc/resolv.conf |\
      tail -n1 | awk '{print $NF}')" |\
      grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' | tail -n1)"
  fi

  # Conferindo se o linux containers esta ativo
  if grep -qE 'container=lxc|container=lxd' /proc/1/environ; then
    LXC="1"
  fi

  # Confere se no arquivo ./ubuntu.cfg AUTOFILL=Y para fazer alguma coisa
  if grep -s "AUTOFILL='Y'" ./ubuntu.cfg; then
    # Cria variavel com o ip do usuario
    USERIP="$($WBIN -ih | awk '{print $3}' | head -n1)"

    # Se for um ip nos padrões definir com ADMINIP
    if [[ "$USERIP" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
      ADMINIP="$USERIP"
    else
      ADMINIP="$(hostname -I | sed -E 's/\.[0-9]+ /.0\/24 /g')"
    fi
    
    # Definindo variaveis no ubuntu.cfg ADMINIP SSH_GRPS pega o nome do usuário a data em changeme e modo verbose
    sed -i "s/FW_ADMIN='/FW_ADMIN='$ADMINIP /" ./ubuntu.cfg
    sed -i "s/SSH_GRPS='/SSH_GRPS='$(id "$($WBIN -ih | awk '{print $1}' | head -n1)" -ng) /" ./ubuntu.cfg
    sed -i "s/CHANGEME=''/CHANGEME='$(date +%s)'/" ./ubuntu.cfg
    sed -i "s/VERBOSE='N'/VERBOSE='Y'/" ./ubuntu.cfg
  fi

  # Carrega as variavies que necesstará em todos os scripts futuros
  source ./ubuntu.cfg

  #fixa os valores das variavies para nao ter mudanças
  readonly ADDUSER
  readonly ARPBIN
  readonly AUDITDCONF
  readonly AUDITD_MODE
  readonly AUDITD_RULES
  readonly AUDITRULES
  readonly AUTOFILL
  readonly CHANGEME
  readonly COMMONACCOUNT
  readonly COMMONAUTH
  readonly COMMONPASSWD
  readonly COREDUMPCONF
  readonly DEFAULTGRUB
  readonly DISABLEFS
  readonly DISABLEMOD
  readonly DISABLENET
  readonly FW_ADMIN
  readonly JOURNALDCONF
  readonly KEEP_SNAPD
  readonly LIMITSCONF
  readonly LOGINDCONF
  readonly LOGINDEFS
  readonly LOGROTATE
  readonly LOGROTATE_CONF
  readonly LXC
  readonly ADMINEMAIL
  readonly NTPSERVERPOOL
  readonly PAMLOGIN
  readonly PSADCONF
  readonly PSADDL
  readonly RESOLVEDCONF
  readonly RKHUNTERCONF
  readonly SECURITYACCESS
  readonly SERVERIP
  readonly SSHDFILE
  readonly SSHFILE
  readonly SSH_GRPS
  readonly SSH_PORT
  readonly SYSCTL
  readonly SYSCTL_CONF
  readonly SYSTEMCONF
  readonly TIMEDATECTL
  readonly TIMESYNCD
  readonly UFWDEFAULT
  readonly USERADD
  readonly USERCONF
  readonly VERBOSE
  readonly WBIN

  # Carrega todos as funções
  for s in ./scripts/*; do
    [[ -f $s ]] || break

    source "$s"
  done

  # Executa todas as funções carregadas. 
  # Ao final de cada função ele alimenta o contador

  f_pre  # funcao de pré script que defini o contador
  f_kernel # Defini maximod e conexões e modo de confidenciabilidade do kernel
  f_firewall # Ativa e ajusta as regras se o firewall ufw tiver instalado
  f_disablenet # Desativa protocolos de redes não utilizados em bastion
  f_disablefs # Desativa alguns sistemas de arquivos
  f_disablemod # Desativa módulos (drivers) desnecessarios para uma vm
  f_systemdconf # Ajusta algumas configuracoes do systemd
  f_resolvedconf # Configura corretamente o resolv.conf e o nsswitch.conf
  f_logindconf # Configura como o sistema deve se comportar com os processos do usuario quando faz login e logout
  f_journalctl # Ajusta o padrao de rotatividade do log e configuracoes do jornald para persistencia e log do syslog
  f_timesyncd # Configurando o time do sistema
  f_fstab # Desmontando coisas necessárias
  f_prelink # Remove o prelink se estiver instalado
  f_aptget_configure # Definindo configurações para o apt-get
  f_aptget # Removendo itens desnecessarios
  f_hosts # Definindo security grou para o hosts
  f_issue # Definindo mensagem para uso do sistema
  f_sudo # configurando parâmetros para o sudo
  f_logindefs # Definições de login
  f_sysctl # Definindo limites do kernel
  f_limitsconf # Definindo limites de recursos
  f_adduser # Defini padrão na hora de adicionar usuário
  f_rootaccess # Configurando acesso root 
  f_package_install # Instalando pacotes
  f_psad # Ativando o Port Scan Attack Detector
  f_coredump # Configurando coredump
  f_usbguard # Instalando e configurando o usbguard
  f_postfix # Restringindo o mail server postifx
  f_apport # Desintalando reports para o ubuntu
  f_motdnews # Desativando o menssage of the day
  f_rkhunter # Ativando o rkhunter para verificar rootkits backdoors e outras falhas de seguranca
  f_sshconfig
  f_sshdconfig
  f_password #
  f_cron
  f_ctrlaltdel # Altera a funcao ctrl alt delete
  f_auditd
  f_aide # Configuracao do sistema de detecção de intrusão Aide
  f_rhosts
  f_users # Deleta alguns usuários do sistema
  f_lockroot # Bloqueia a conta root
  f_package_remove # Remove alguns pacotes não necessários ou inseguros
  f_suid # Remove permissoes especiais 
  f_restrictcompilers
  f_umask # Defini algumas permissões padrões
  f_path # Modifica o path do sistema
  f_aa_enforce #q Ativa o apparmor
  f_aide_post # Inicia o aide
  f_aide_timer # Colocando o aide para checagem diária
  f_aptget_noexec
  f_aptget_clean # Removendo pacotes não utilizados
  f_systemddelta # Encontra arquivos de configuracoes que foram substituidos
  f_post # Processo de finização
  f_checkreboot # Confere se um reboot é necessário

  echo
}

# Entrega o log
LOGFILE="hardening-$(hostname --short)-$(date +%y%m%d).log"
echo "[HARDENING LOG - $(hostname --fqdn) - $(LANG=C date)]" >> "$LOGFILE"

main "$@" | tee -a "$LOGFILE"
