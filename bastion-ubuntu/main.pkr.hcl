source "amazon-ebs" "aws" {

  access_key = var.access_key
  secret_key = var.secret_key

  ami_name      = local.full_image_name
  instance_type = var.instance_type
  region        = var.region

  source_ami = data.amazon-ami.ami.id

  ssh_username            = var.ssh_username
  ssh_timeout             = var.ssh_timeout
  ssh_keep_alive_interval = var.ssh_keep_alive_interval
  ssh_pty                 = var.ssh_pty

  ami_regions       = var.ami_regions
  shutdown_behavior = "terminate"

  # tags = merge(var.tags, local.extra_tags)
}

build {

  name = "bastion"

  sources = ["source.amazon-ebs.aws"]

  provisioner "file" {
    destination = "/tmp"
    source      = "./files/hardening"
  }
  provisioner "shell" {
    script = "./files/aws_configure.sh"
  }
}