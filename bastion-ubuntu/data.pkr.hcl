data "amazon-ami" "ami" {
  filters = {
    name                = var.ami_name
    virtualization-type = var.virtualization
    root-device-type    = "ebs"
  }
  owners      = ["${var.ami_owner}"]
  most_recent = true
  region      = var.region
}