# bastion-ubuntu

Esse projeto gera uma imagem utilizando do ubuntu com hardening para a criação de um bastion host através de scripts.

## Requisitos

- Packer Instalado
- Conta na AWS

## Chaves da AWS

É necessário exportar as chaves iam com permissão para criação de imagem.

```bash
export AWS_ACCESS_KEY_ID=<AccessKeyID> 
export AWS_SECRET_ACCESS_KEY=<SecretAccessKey>
```

## Buildar a Imagem AMI

O packer sobe uma ec2 temporária na aws, com um ubuntu de imagem base definido no [main.pkr.hcl](./packer/main.pkr.hcl) no bloco abaixo. Depois executa todos os comandos dos scripts copiados para dentro desta ec2 que estará na pasta `/tmp/hardening`. Este diretório contem todos o conteúdo da pasta [hardening](./packer/aws_configure.sh)

```yaml
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/hvm-ssd/*ubuntu-jammy-22.04-amd64-server*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
```

Este filtro vai buscar uma ami de propriedade da Canonical (099720109477) cujo nome é  definido em name.
Para procurar outras ami de base pode usar <https://cloud-images.ubuntu.com/locator/ec2/>.

Para buildar a imagem entre na pasta [packer](./packer) e execute os seguinte comandos como mostra no exemplo abaixo. Nesse caso é passado o arquivo de configuração das variáveis que fica dentro de [vars](./packer//vars/). Para regiões diferentes e imagens diferentes crie um novo arquivo e aponte ele.

```bash
# Para fazer download dos plugins
packer init .
# Para buildar a imagem
packer build -var-file="./vars/aws.auto.pkrvars.hcl" .
### ....
#==> Builds finished. The artifacts of successful builds are:
#--> amazon-ebs.node: AMIs were created:
#us-east-1: ami-09e94a872c3b5bc19  << ISSO QUE QUEREMOS
```
