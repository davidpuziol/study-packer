locals {
  timestamp = formatdate("DD-MMM-YYYY-hh-mm-ZZZ", timestamp())

  full_image_name = var.image_tag == "" ? "${var.image_name}-${local.timestamp}" : "${var.image_name}-${var.image_tag}"

  extra_tags = {
    Name = var.image_name
  }
}