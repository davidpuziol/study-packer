variable "access_key" {
  description = "AWS Access Key ID"
  type        = string
  default     = env("AWS_ACCESS_KEY_ID")
}

variable "secret_key" {
  description = "AWS Secret Key ID"
  type        = string
  default     = env("AWS_SECRET_ACCESS_KEY")
}

variable "region" {
  description = "Region where will create the image"
  type        = string
}

variable "ssh_username" {
  description = "Name to acess ssh"
  type        = string
}

variable "ssh_port" {
  description = "Port to access ssh"
  type        = number
  default     = 22
}

variable "ssh_pty" {
  description = "PTY will be requested for the SSH connection"
  type        = string
  default     = true
}

variable "ssh_timeout" {
  description = "Time to end the session when not used"
  type        = string
  default     = "5m"
}

variable "ssh_keep_alive_interval" {
  description = "Keep Alive"
  type        = string
  default     = "15s"
}

variable "instance_type" {
  description = "Instante type to create the image"
  type        = string
  default     = "t3.medium"
}

variable "virtualization" {
  description = "Virtualization Type"
  type        = string
  default     = "hvm"
}

variable "ami_name" {
  description = "Ami name used to filter"
  type        = string
}

variable "ami_owner" {
  description = "Image owner used to filter"
  type        = string
}

variable "image_name" {
  description = "Future Ami Name"
  type        = string
}

variable "image_tag" {
  description = "Tag to concat in image_name"
  type        = string
  default     = ""
}

variable "ami_regions" {
  description = "Regions to copy new image"
  type        = list(string)
  default     = []
}