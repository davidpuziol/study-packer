# Exemplo Azure

Nesse exemplo vamos utilizar o ansible para configurar para conigurar um centos na azure.

O projeto se encontra em [azure-ansible](https://gitlab.com/davidpuziol/study-packer/-/tree/main/azure-ansile?/)

- Hardening usando uma collection pronta
- Update do sistema operacional
- Resolv.conf
- MOTD
- Instalação do docker
- NTP

A proposta é não utilizar scripts, somente playbooks do ansible. Não vou explicar playbooks do ansible, mas como podemos utilizar.

Existe o plugin do ansible, mas no caso deste projeto vamos copiar os playbooks para dentro da vm do source e rodar os playbooks de dentro da máquina para configurá-la, por isso o uso de localhost.

Para que o playbook execute é necessário ter o ansible instalado e as collections necessárias. Por isso foi separado um provisioner shell para primeiro deixar tudo preparado para depois somente executar o playbook.

## Source Azure

No caso do source para a azure é necessário atentar-se a alguns detalhes

uma vez que `use_azure_cli_auth = true`, previamente você precisa executar o comando `az login`. Isso irá facilitar não ter que passar os seguintes parâmetros que deixei comentado caso necessário.

- client_id       = var.client_id
- client_secret   = var.client_secret
- tenant_id       = var.tenant_id
- subscription_id = var.subscription_id

Observe que para esses parâmetros preferi pegar eles de variáveis e estas usam valores exportados anteriomente. Isso já ajudará no caso de pipelines.

exemplo:

```properties
variable "client_secret" {
  description = "AZURE Client Secret"
  type        = string
  default     = env("AZURE_CLIENT_SECRET")
}
```

Nesse caso seria necessário exportar.

```bash
export ARM_CLIENT_ID=xxxxx-xxxx-xxxx-xxx-xxx
export ARM_CLIENT_SECRET=xxxxx-xxxx-xxxx-xxx-xxx
export ARM_SUBSCRIPTION_ID=xxxxx-xxxx-xxxx-xxx-xxx
export ARM_TENANT_ID=xxxxx-xxxx-xxxx-xxx-xxx
```

As clouds nomeiam as regiões e vms de forma diferente. Por isso criei um map para ambos e somente passamos 1 valor e automaticamente ele já irá buscar o nome correto. `Fica a dica`. Isso é muito bom para vários sources ao mesmo tempo.

Um outro detalhe é que escolhi uma imagem que não é community, ou seja, free, logo é necessário passar o plan também.

```properties
source "azure-arm" "azure" {

  # client_id       = var.client_id
  # client_secret   = var.client_secret
  # tenant_id       = var.tenant_id
  # subscription_id = var.subscription_id

  use_azure_cli_auth = true

  plan_info {
    plan_publisher = var.image_publisher
    plan_product   = var.image_offer
    plan_name      = var.image_sku
  }

  os_type         = "Linux"
  image_publisher = var.image_publisher
  image_offer     = var.image_offer
  image_sku       = var.image_sku

  managed_image_resource_group_name = "packer"
  managed_image_name                = local.full_image_name

  azure_tags = var.vm_tags

# Veja os maps em locals e conditions nas variáveis.

  location = local.map_regions[var.region]["azure"]
  vm_size  = var.instance_sizes["azure"]
}
```

Rodando o projeto

```bash
packer init . --upgrade
packer build "./vars/centos7.pkr.hcl" .
```
