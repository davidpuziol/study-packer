locals {
  timestamp = formatdate("DD-MMM-YYYY-hh-mm-ZZZ", timestamp())
  full_image_name = var.image_version == "" ? "${var.image_name}-${local.timestamp}" : "${var.image_name}-${var.image_version}"
}