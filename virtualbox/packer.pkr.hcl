packer {
  required_version = ">= 1.8.5"
  required_plugins {
      virtualbox = {
        version = "~> 1"
        source  = "github.com/hashicorp/virtualbox"
      }
      libvirt = {
        version = ">= 0.5.0"
        source  = "github.com/thomasklein94/libvirt"
      }
  }
}