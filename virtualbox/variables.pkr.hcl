variable "vm_tags" {
  description = "A map of tags for the image"
  type        = map(string)
  default = {
    CreatedBy = "Packer"
  }
}

variable "image_name" {
  description = "Name"
  type        = string
}

variable "image_iso_url" {
  description = "Iso url"
  type        = string
}

variable "image_iso_checksun" {
  description = "Iso checksun. Use sha256:XXXXXX or md5:XXXXXX"
  type        = string
}


variable "os_type" {
  description = "Operatinal System Type using on virtualbox"
  type        = string
}

variable "image_version" {
  description = "Version to concat in image_name"
  type        = string
  default     = ""
}