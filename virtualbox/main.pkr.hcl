source "virtualbox-iso" "virtualbox" {

  # Especificações da VM
  ## Imagens utilizadas que serão carregas no cdrom
  iso_url       = var.image_iso_url
  iso_checksum  = var.image_iso_checksun

  guest_os_type         = var.os_type ## Template do SO
  vm_name               = local.full_image_name #{{ .Name }} irá usar esse nome
  ## Definições de hardware
  vboxmanage            = [[ "modifyvm", "{{ .Name }}", "--memory", "2024"], [ "modifyvm", "{{ .Name }}", "--cpus", "2" ]]
  disk_size             = "100000"
  hard_drive_interface  = "sata"

  ## Comandos de instalação do sistema operacional assim que carregar
  http_directory   = "http"
  boot_command  = ["<tab> text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg<enter><wait>"]

  ## Usar para o mesmo usuario criado no http/ks.cfg
  ssh_password     = "rocky"
  ssh_username     = "rocky"
  ssh_timeout      = "20m"

  headless         = "true" # Remover o aparecimento do console quando inicia
  
  output_directory = "output-virtualbox-iso"
  shutdown_command = "sudo /sbin/halt -p"
}

build {
  sources = ["sources.virtualbox-iso.virtualbox"]
}