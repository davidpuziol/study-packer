# Comandos

## Sobre a linguagem

É possível utilizar tanto Json quando a linguagem HCL2 da hashicorp.

>Recomenda-se fortemente utilizar o HCL2 ao invés de usar o Json. O HCL2 é muito parecido com o terraform.

A documentação mostrará exemplo na maioria dos casos em ambas as linguagem, mas a própria Hashicorp recomenda quem esta utilizando Json migrar para o HCL2.

Alguns comandos não funcionam com arquivos json:

- init
- fmt
- uso de funções

Uma explicação melhor sobre o modelo HCL estará disponíbel em [Modelo](./2%20-%20Modelo.md).

## Comandos da CLI

Para facilitar instale o auto complete do Packer para que o tab funcine e auto complete comandos.

```bash
packer -autocomplete-instal
```

Faça um tour com o packer cli e veja como é simples

```bash
packer -h        
Usage: packer [--version] [--help] <command> [<args>]

Available commands are:
    build           build image(s) from template
    console         creates a console for testing variable interpolation
    fix             fixes templates from old versions of packer
    fmt             Rewrites HCL2 config files to canonical format
    hcl2_upgrade    transform a JSON template into an HCL2 configuration
    init            Install missing plugins or upgrade plugins
    inspect         see components of a template
    plugins         Interact with Packer plugins and catalog
    validate        check that a template is valid
    version         Prints the Packer version
```

### Init

O comando `packer init`é usado para baixar os binários de plug-in do Packer. Este é o primeiro comando que deve ser executado ao trabalhar com um modelo novo ou existente. Este comando é sempre seguro para ser executado várias vezes. Embora as execuções subsequentes possam apresentar erros, este comando nunca excluirá nada.

>`packer init` somente busca binários de projetos publicos do github.

`packer init -upgrade`irá atualizar os plugins

Falaremos sobre plugins mais adiante

### Format

O comando `packer fmt` é usado para formatar arquivos de configuração HCL2 para um formato e estilo canônicos. Arquivos Json não são modificados.

Bom para ser usado em pipelines.

```bash
# Só mostra arquivos que irá sofrer diferença para os arquivos do diretório corrent
packer fmt -check .
aws.pkr.hcl

# Faz um diff e mostra o que vai acontecer nos arquivos do diretório corrente
packer fmt -diff . 
aws.pkr.hcl
--- old/aws.pkr.hcl
+++ new/aws.pkr.hcl
@@ -25,7 +25,7 @@
 
 build {
   sources = ["source.amazon-ebs.teste"]
-  
+
   provisioner "file" {
     destination = "/tmp"
     source      = "./teste"
```

### Validate

O comando `packer validate` é usado para validar a sintaxe e a configuração de um modelo. O comando retornará um status de saída zero em caso de sucesso e um status de saída diferente de zero em caso de falha.

Se todas as variáveis tiverem seu valor default ou for estaticamente definida, mente o comando `packer validate`será suficiente.

Porém se alguma variável não tiver o valor padrão definido, é necessário passar ou o valor da variável ou o arquivo de configuração das variáveis (que é o mais recomendado)

```bash
# comando executado dentro do diretório que contéms os modelos
packer validate .
The configuration is valid.

# passando um arquivo de configurações dos valores das variáveis.
packer validate -var-file="./vars/meuprojeto.auto.pkrvars.hcl" .
The configuration is valid.

# ou somente a sintaxxe
packer validate -syntax-only .
Syntax-only check passed. Everything looks okay.
```

### Build

O comando `packer build` pega um modelo e executa todas as compilações dentro dele para gerar um conjunto de artefatos. As várias compilações especificadas em um modelo são executadas em paralelo, a menos que especificado de outra forma. E os artefatos criados serão gerados no final da compilação.

Para qualquer comando abaixo pode ser usado o parametro `-debug` para ajudar a resolver problemas.

Como mensionado é possível execução em paralelo caso esteja por exemplo criando uma imagem para a aws e azure ao mesmo tempo e isso é habilitado por padrão, mas possível ser desativado caso necessário.

Gosto muito de redirecionar a saída para um arquivo de log usando um `| tee > build.log` ao fim do comando. O terminal acaba sumindo com algumas linhas importantes e acaba sendo bom ter toda a saída disponível para analise.

```bash
packer build -var-file="./vars/teste.auto.pkrvars.hcl" . | tee log
amazon-ebs.teste: output will be in this color.

==> amazon-ebs.teste: Prevalidating any provided VPC information
==> amazon-ebs.teste: Prevalidating AMI Name: teste-20230304194337
    amazon-ebs.teste: Found Image ID: ami-09a187f07b7a0769e
==> amazon-ebs.teste: Creating temporary keypair: packer_64039f69-6288-44c4-8806-cf8929a15f90
==> amazon-ebs.teste: Creating temporary security group for this instance: packer_64039f6e-afba-1c56-8ddb-d7adfcc6da30
...
```

Packer build leva um argumento. Quando um `diretório` é passado, todos os arquivos na pasta com um nome que termina com .pkr.hcl ou .pkr.json serão analisados. Quando um `arquivo` é passado somente este será analisado.
