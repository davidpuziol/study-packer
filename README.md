# Packer

![packer](./pics/packer.svg)

Repositório para estudo do [Packer](https://www.packer.io/)

[Documentação oficial](https://developer.hashicorp.com/packer/docs)
[Repositório oficial do packer](https://github.com/hashicorp/packer)

## O que é Packer?

O Packer é uma ferramenta de código aberto que permite criar imagens de máquinas idênticas para várias plataformas a partir de um único modelo de código-fonte. O Packer pode criar imagens douradas para usar em pipelines de imagem.

- Binário leve
- Alto desempenho
- Escrito em go
- Simples e eficas
- roda em todos os principais sistemas operacionais
- Cria imagens de máquina para várias plataformas em paralelo.

> O packer não substitui o gerenciamento de configuração como Ansible, mas trabalha em conjunto.

![Packer Diagram](https://www.datocms-assets.com/2885/1662053782-dev-dot-product-landing-what-is-packer-diagram.png)

## O que é uma Imagem?

Uma imagem de máquina é uma unidade estática única que contém um sistema operacional pré-configurado e um software instalado que é usado para criar rapidamente novas máquinas em execução. Os formatos de imagem de máquina mudam para cada plataforma. Alguns exemplos incluem AMIs para EC2, arquivos VMDK/VMX para VMware, exportações OVF para VirtualBox, etc.

## Instalação do Packer

Como mensionamento anteriormente o packer nada mais é que um binário e possuí sua própria cli.
Precisa estar no path do sistema para ser encontrado via terminal, logo, deve-se mover o binário para uma pasta que esteja no path do sistema ou adicionar o local de instalação no path.

[Packer Installation Documentation](https://developer.hashicorp.com/packer/tutorials/docker-get-started/get-started-install-cli)

Por exemplo uma instalação no Ubuntu:

```bash
 wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
 echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
 sudo apt update && sudo apt install packer
```

Ou pode baixar o binário direto do [repositório](https://github.com/hashicorp/packer/releases).

## Estudo

[1 - Comandos](./1%20-%20Comandos.md) \
[2 - Modelo](./2%20-%20Modelo.md) \
[3 - Projeto](./4%20-%20Projeto-Base.md) \
[4 - Variáveis.md](./5%20-%20Variaveis.md) \
[5 - Locals e Functions](./6%20-%20Locals%20e%20Functions.md) \
[6 - Plugins](./7%20-%20Plugins.md) \
[7 - Build](./8%20-%20Build.md) \
[8 - Exemplo AWS](./9%20-%20Exemplo%20AWS.md) \
[9 - Exemplo Azure](./10%20-%20Exemplo%20Azure.md)
