# Projeto

Vamos criar uma estrutura da seguinte forma.

```bash
mkdir -p ./project/{vars,scripts,files,services,ansible}

touch ./project/{main.pkr.hcl,packer.pkr.hcl,variables.pkr.hcl,locals.pkr.hcl,data.pkr.hcl}

tree project 
project
├── files
├── locals.pkr.hcl
├── main.pkr.hcl
├── packer.pkr.hcl
├── scripts
├── services
├── variables.pkr.hcl
└── vars

5 directories, 4 files
```

Diretórios:

- `files`: Esse diretório irá conter todos os arquivos que não são scripts
- `scripts`: Todos os scripts que iremos executar colocaremos aqui
- `services`: Todos os serviços que iremos ativar no sistema operacional poderemos colocar aqui
  - Esses serviços são uma manobra para lançar alguma configuração após a inicialização da imagem
- `vars`: Aqui teremos os arquivos de entrada para preencher as variáveis que não possuem valores default ou alguma que queremos redefinir
- `ansible`: Poderíamos ter um conjunto de playbooks para serem executados com ansible nesta pasta que não definimos, mas fica a dica.

Arquivos de base do packer lembrando que tem que ser .pkr.hcl

- `main.pkr.hcl` Neste arquivo declararemos os sources e o bloco build
- `packer.pkr.hcl` Neste arquivo vamos declarar configurações para o packer, como versão e plugins
- `variables.pkr.hcl` Neste arquivo declararemos as variáveis que iremos utilizar
- `locals.pkr.hcl` Caso alguma variável seja definida em tempo de execução por uma função ou interpolação será criadas neste arquivo.
- `data.pkr.hcl`Esse arquivo será usado para poder entregar varias possiveis imagens.

O projeto seguirá com explições nos próximos passos. A idéia é estudar ir criando o projeto enquanto aprende.